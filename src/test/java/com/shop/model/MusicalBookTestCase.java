package com.shop.model;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert. assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class MusicalBookTestCase {
	protected MusicalBook musicalBook;
	
	@Before
	public void setUp () {
		List<String> listOfSound = new ArrayList<String>();
		String name = "Name song: ";
		for(int i=0;i<10;i++) {
			listOfSound.add(name+i);
		}

    	musicalBook= new MusicalBook("Maitre Gims super star", 3, 10.99f, 7,4, "Jeannet Padegou", "childedit",2002,19,listOfSound, 12,3);
	}
	
	@After
	public void tearDown () {
	}
    @Test
    public void createEmptyMusicalBook() throws Exception {
        MusicalBook musicalBook1=new MusicalBook();

        assertTrue(true);
    }
    @Test
    public void createParameterMusicalBook() throws Exception {
		List<String> listOfSound = new ArrayList<String>();
		String name = "Name song: ";
		for(int i=0;i<10;i++) {
			listOfSound.add(name+i);
		}

    	MusicalBook musicalBook1= new MusicalBook("Maitre Gims super star", 3, 10.99f, 7,4, "Jeannet Padegou", "childedit",2002,19,listOfSound, 12,3);
        assertTrue(true);
    }
    @Test
    public void createBookMusicalBook() throws Exception {
    	Book book=new Book("oui oui va a la plage", 1, 1.99f, 3 ,52, "Bernard Minet", "childedit",1999,6);
        MusicalBook musicalBook1=new MusicalBook(book);

        assertTrue(true);
    }
	@Test
	public void testLifetime() throws Exception {
		int lifetime =251;
		musicalBook.setLifetime(lifetime);
		assertEquals(lifetime, musicalBook.getLifetime());
	}
	@Test
	public void testNbrBattery() throws Exception {
		int nbrBattery =251;
		musicalBook.setNbrBattery(nbrBattery);
		assertEquals(nbrBattery, musicalBook.getNbrBattery());
	}
	@Test
	public void testListOfSound() throws Exception {
		List<String> listOfSound = new ArrayList<String>();
		String name = "Nom chanson: ";
		for(int i=0;i<10;i++) {
			listOfSound.add(name+i);
		}
		musicalBook.setListOfSound(listOfSound);
		assertTrue(listOfSound.equals(musicalBook.getListOfSound()));
	}
}
