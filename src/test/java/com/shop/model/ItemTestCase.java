package com.shop.model;


import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert. assertEquals;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ItemTestCase {
	protected Item item;
	
	@Before
	public void setUp () {
		item = new Item();
	}
	
	@After
	public void tearDown () {
	}
    @Test
    public void createEmptyConsumable() throws Exception {
        Item item=new Item();

        assertTrue(true);
    }
    @Test
    public void createParameterItem() throws Exception {
        Item item=new Item("item", 1, 1.99f, 3);

        assertTrue(true);
    }

	@Test
	public void testId() throws Exception {
		int id =1;
		item.setId(id);
		assertEquals(1, item.getId());
	}
	@Test
	public void testPrice() throws Exception {
		float price =1.9f;
		item.setPrice(price);
		assertEquals(1.9f, item.getPrice(),0.0f);
	}
	@Test
	public void testNbrElt() throws Exception {
		int nbrElt =2;
		item.setNbrElt(nbrElt);
		assertEquals(2, item.getNbrElt());
	}
	@Test
	public void testName() throws Exception {
	String name ="TEST1";
	item.setName(name);
	assertTrue("TEST1".equals(item.getName()) );
	}
	@Test
	public void testDisplay() throws Exception {

	item = new Item("TEST1",1,1.9f,2);
	String toto="id = "+item.getId()+" , name = "+item.getName()+" , price = "+item.getPrice()+" , nbrElt = "+item.getNbrElt();
	assertTrue(toto.equals(item.display()));
	
	}
}