package com.shop.model;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert. assertEquals;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class BookTestCase {
	protected Book book;
	
	@Before
	public void setUp () {
		book = new Book();
	}
	
	@After
	public void tearDown () {
	}
    @Test
    public void createEmptyBook() throws Exception {
        Book book=new Book();

        assertTrue(true);
    }
    @Test
    public void createParameterBook() throws Exception {
        Book book=new Book("oui oui va a la plage", 1, 1.99f, 3 ,52, "Bernard Minet", "childedit",1999,6);

        assertTrue(true);
    }
    public void createItemBook() throws Exception {
    	Item item= new Item("item", 1, 1.99f, 3);
        Book book=new Book(item);

        assertTrue(true);
    }
	@Test
	public void testNbPage() throws Exception {
		int nbPage =251;
		Item item= new Item("item", 1, 1.99f, 3);
		book = new Book(item);
		book.setNbPage(nbPage);
		assertEquals(nbPage, book.getNbPage());
	}
	@Test
	public void testAuthor() throws Exception {
		String author = "BHL";
		Item item= new Item("item", 1, 1.99f, 3);
		book = new Book(item);
		book.setAuthor(author);
		assertTrue(author.equals(book.getAuthor()));
	}
	@Test
	public void testPublisher() throws Exception {
		String publisher = "Hachet";
		Item item= new Item("item", 1, 1.99f, 3);
		book = new Book(item);
		book.setPublisher(publisher);
		assertTrue(publisher.equals(book.getPublisher()));
	}
	
	@Test
	public void testYear() throws Exception {
		int year =1999;
		Item item= new Item("item", 1, 1.99f, 3);
		book = new Book(item);
		book.setYear(year);
		assertEquals(year, book.getYear());
	}
	@Test
	public void testAge() throws Exception {
		int age =18;
		Item item= new Item("item", 1, 1.99f, 3);
		book = new Book(item);
		book.setAge(age);
		assertEquals(age, book.getAge());
	}
}
