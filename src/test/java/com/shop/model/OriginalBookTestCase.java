package com.shop.model;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert. assertEquals;
import static org.junit.Assert.assertFalse;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class OriginalBookTestCase {
	protected OriginalBook originalBook;
	
	@Before
	public void setUp () {
		originalBook=new OriginalBook("oui oui va a la plage", 1, 1.99f, 3 ,52, "Bernard Minet", "childedit",1999,6,true);
	}
	
	@After
	public void tearDown () {
	}
    @Test
    public void createEmptyOriginalBook() throws Exception {
        OriginalBook originalBook1=new OriginalBook();

        assertTrue(true);
    }
    @Test
    public void createParameterOriginalBook() throws Exception {
        OriginalBook originalBook1=new OriginalBook("oui oui va a la plage", 1, 1.99f, 3 ,52, "Bernard Minet", "childedit",1999,6,true);

        assertTrue(true);
    }
    @Test
    public void createBookOriginalBook() throws Exception {
    	Book book=new Book("oui oui va a la plage", 1, 1.99f, 3 ,52, "Bernard Minet", "childedit",1999,6);
        OriginalBook originalBook1=new OriginalBook(book);

        assertTrue(true);
    }
	@Test
	public void testNumeric() throws Exception {
		
		originalBook.setNumeric(false);
		assertFalse(originalBook.isNumeric());
	}
}
