package com.shop.model;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class PaperTestCase {
	protected Paper paper;
	
	@Before
	public void setUp () {
		paper = new Paper();
	}
	
	@After
	public void tearDown () {
	}
    @Test
    public void createEmptyPaper() throws Exception {
        Paper paper=new Paper();

        assertTrue(true);
    }
    @Test
    public void createParameterPaper() throws Exception {
        Paper paper=new Paper("Canson", 1, 1.99f, 3,4,"MEDIUM",90f);

        assertTrue(true);
    }
    public void createItemPaper() throws Exception {
    	Consumable consumable= new Consumable("Canson", 1, 1.99f, 3,4);
        Paper paper=new Paper();

        assertTrue(true);
    }
    @Test
	public void testQuality() throws Exception {
		String quality ="MEDIUM";
		Consumable consumable= new Consumable("Canson", 1, 1.99f, 3,4);
		paper = new Paper();
		paper.setQuality(quality);
		assertEquals(quality, paper.getQuality());
	}
}
