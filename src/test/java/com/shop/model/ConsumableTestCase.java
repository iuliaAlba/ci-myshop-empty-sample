package com.shop.model;


import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert. assertEquals;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.shop.controller.Storage;

public class ConsumableTestCase {
	protected Consumable consumable;
	
	@Before
	public void setUp () {
		consumable = new Consumable();
	}
	
	@After
	public void tearDown () {
	}
    @Test
    public void createEmptyConsumable() throws Exception {
        Consumable consumable=new Consumable();

        assertTrue(true);
    }
    @Test
    public void createParameterConsumable() throws Exception {
        Consumable consumable=new Consumable("item", 1, 1.99f, 3,4);

        assertTrue(true);
    }
    public void createItemConsumable() throws Exception {
    	Item item= new Item("item", 1, 1.99f, 3);
        Consumable consumable=new Consumable(item);

        assertTrue(true);
    }
	@Test
	public void testQuantity() throws Exception {
		int qty =2;
		Item item= new Item("item", 1, 1.99f, 3);
		consumable = new Consumable(item);
		consumable.setQuantity(qty);
		assertEquals(qty, consumable.getQuantity());
	}
}