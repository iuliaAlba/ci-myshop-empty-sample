package com.shop.model;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert. assertEquals;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class BookToTouchTestCase {
	protected BookToTouch bookToTouch;
	
	@Before
	public void setUp () {
		bookToTouch=new BookToTouch("oui oui va a la plage", 1, 1.99f, 3 ,52, "Bernard Minet", "childedit",1999,6,"Acier",20);
	}
	
	@After
	public void tearDown () {
	}
    @Test
    public void createEmptyBookToTouch() throws Exception {
        BookToTouch bookToTouch=new BookToTouch();

        assertTrue(true);
    }
    @Test
    public void createParameterBookToTouch() throws Exception {
        BookToTouch bookToTouch=new BookToTouch("oui oui va a la plage", 1, 1.99f, 3 ,52, "Bernard Minet", "childedit",1999,6,"Acier",20);

        assertTrue(true);
    }
    @Test
    public void createBookBookToTouch() throws Exception {
    	Book book=new Book("oui oui va a la plage", 1, 1.99f, 3 ,52, "Bernard Minet", "childedit",1999,6);
        BookToTouch BookToTouch=new BookToTouch(book);

        assertTrue(true);
    }
	@Test
	public void testDurability() throws Exception {
		int durability =251;
		bookToTouch.setDurability(durability);
		assertEquals(durability, bookToTouch.getDurability());
	}
	@Test
	public void testtMaterial() throws Exception {
		String material = "bois";
		bookToTouch.setMaterial(material);
		assertTrue(material.equals(bookToTouch.getMaterial()));
	}
}
