package com.shop.model;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert. assertEquals;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class PuzzleBookTestCase {
	protected PuzzleBook puzzleBook;
	
	@Before
	public void setUp () {
		puzzleBook=new PuzzleBook("oui oui va a la plage", 1, 1.99f, 3 ,52, "Bernard Minet", "childedit",1999,6,500);
	}
	
	@After
	public void tearDown () {
	}
    @Test
    public void createEmptyPuzzleBook() throws Exception {
        PuzzleBook puzzleBook1=new PuzzleBook();

        assertTrue(true);
    }
    @Test
    public void createParameterPuzzleBook() throws Exception {
        PuzzleBook puzzleBook1=new PuzzleBook("oui oui va a la plage", 1, 1.99f, 3 ,52, "Bernard Minet", "childedit",1999,6,300);

        assertTrue(true);
    }
    @Test
    public void createBookPuzzleBook() throws Exception {
    	Book book=new Book("oui oui va a la plage", 1, 1.99f, 3 ,52, "Bernard Minet", "childedit",1999,6);
        PuzzleBook puzzleBook1=new PuzzleBook(book);

        assertTrue(true);
    }
	@Test
	public void testNbrPieces() throws Exception {
		int nbrPieces =1000;
		puzzleBook.setNbrPieces(nbrPieces);
		assertEquals(nbrPieces, puzzleBook.getNbrPieces());
	}
}
