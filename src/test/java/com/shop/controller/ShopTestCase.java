package com.shop.controller;


import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert. assertEquals;
import static org.junit.Assert.assertFalse;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.shop.model.Book;
import com.shop.model.BookToTouch;
import com.shop.model.Consumable;
import com.shop.model.Item;
import com.shop.model.MusicalBook;
import com.shop.model.OriginalBook;
import com.shop.model.Paper;
import com.shop.model.Pen;
import com.shop.model.PuzzleBook;

public class ShopTestCase {
	protected Shop shop;
	
	@Before
	public void setUp () {
		
		List<String> listOfSound = new ArrayList<String>();
		listOfSound.add("sappe comme jamais");
		OriginalBook originalBook= new OriginalBook("oui oui va a la plage", 1, 1.99f, 3 ,52, "Bernard Minet", "childedit",1999,6,false);
    	PuzzleBook puzzleBook= new PuzzleBook("Petit chaton tout mignon", 2, 22.99f, 4,2, "Brigitte Vioc", "ForOldedit",2001,20,300);
    	MusicalBook musicalBook= new MusicalBook("Maitre Gims super star", 3, 10.99f, 7,4, "Jeannet Padegou", "childedit",2002,19,listOfSound, 12,3);
    	BookToTouch bookToTouch= new BookToTouch("Livre toutouch", 4, 9.10f, 3,5, "Jesus sourcommeunpo", "MuseEdit",2011,10,"bois",15);
    	Pen pen = new Pen("Bic bleu", 5, 0.99f, 10, 100, "bleu",2);
    	Paper paper = new Paper("canson",6, 2.00f, 20, 50, "haute", 90f);
    	Storage storage = new Storage();
		storage.addItem(originalBook);
		storage.addItem(puzzleBook);
		storage.addItem(musicalBook);
		storage.addItem(bookToTouch);
		storage.addItem(pen);
		storage.addItem(paper);
		shop= new Shop(storage, 1000.00f);
	}
	
	@After
	public void tearDown () {
	}
    @Test
    public void createEmptyShop() throws Exception {
        Shop shop=new Shop();

        assertTrue(true);
    }
    @Test
    public void createParameterShop() throws Exception {
    	
		Item item= new Item("name", 1, 1.99f, 3);
    	Storage storage = new Storage();
		storage.addItem(item);
		Shop shop=new Shop(storage,1000);

        assertTrue(true);
    }
    
	@Test
	public void testCash() throws Exception {
		float cash =1.9f;
		shop.setCash(cash);
		assertEquals(1.9f, shop.getCash(),0.0f);
	}
	
	@Test
	public void testStorage() throws Exception {
		Item item= new Item("name", 1, 1.99f, 3);
    	Storage storage = new Storage();
		storage.addItem(item);
		shop.setStorage(storage);
		assertTrue(shop.getStorage().equals(storage));
	}
	
	@Test
	public void testDisplayCash() throws Exception {
		String toto="cash = "+ shop.getCash();
		assertTrue(toto.equals(shop.displayCash()));
	
	}
	@Test
	public void testBuy() throws Exception {
		float cash = shop.getCash();
		String name = "TEST1";
		Item item = new Item(name,1,1.9f,2);
		shop.buy(item);
		float cash2= cash - item.getPrice();
		assertEquals(cash2, shop.getCash(),0.0f);
		assertTrue(shop.isItemAvailable(name));
		
	
	}
	@Test
	public void testSell() throws Exception {
		String itemName = "oui oui va a la plage"; 
		float cash = shop.getCash();
		float cashSailed= cash + shop.getStorage().getItem(itemName).getPrice();
		shop.sell(itemName);
		
		assertEquals(cashSailed, shop.getCash(),0.0f);
		assertFalse(shop.isItemAvailable(itemName));
		
	
	}
	
	@Test
	public void testIsItemAvailable() throws Exception {
		assertTrue(shop.isItemAvailable("Bic bleu"));
		assertFalse(shop.isItemAvailable("Bic rouge"));
	}
	@Test
	public void testGetAgeForBook() throws Exception {
		String nameBook = "oui oui va a la plage";
		int ageOuiOui = 6; // voir dans before
		String namePen = "Bic bleu";
		
		assertEquals(ageOuiOui,shop.getAgeForBook(nameBook));
		assertEquals(-1,shop.getAgeForBook(namePen));
	}

	@Test
	public void testGetAllBook() throws Exception {
    	List<Book> lBook = new ArrayList<Book>();
    	
    	lBook.add((Book)shop.getStorage().getItem("oui oui va a la plage"));
    	lBook.add((Book)shop.getStorage().getItem("Petit chaton tout mignon"));
    	lBook.add((Book)shop.getStorage().getItem("Maitre Gims super star"));
    	lBook.add((Book)shop.getStorage().getItem("Livre toutouch"));
		
    	boolean b = (boolean) lBook.equals(shop.getAllBook());
		assertTrue(b);
	}

	@Test
	public void testGetINbtemInStorage() throws Exception {
    	String nameItem = "oui oui va a la plage";
	
		assertEquals(3,shop.getINbtemInStorage(nameItem));
	}
	
	@Test
	public void testGetQuantityPerConsumable() throws Exception {
    	String nameItem = "oui oui va a la plage";
    	String nameConsumable = "Bic bleu";
    	
		assertEquals(-1,shop.getQuantityPerConsumable(nameItem));
		assertEquals(100,shop.getQuantityPerConsumable(nameConsumable));
	}

}