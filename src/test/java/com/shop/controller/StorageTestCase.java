package com.shop.controller;


import static org.junit.Assert. assertEquals;

import java.util.HashMap;
import java.util.Map;

import static junit.framework.TestCase.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.shop.model.Item;

public class StorageTestCase {
	protected Storage storage;
	
	@Before
	public void setUp () {
		storage = new Storage();
	}
	
	@After
	public void tearDown () {
	}
	
    @Test
    public void createEmptyStorage() throws Exception {
        Storage storage=new Storage();

        assertTrue(true);
    }
	
	@Test
	public void testItem() throws Exception {
		String name = "item";
		Item item= new Item(name, 1, 1.99f, 3);
		storage.addItem(item);
		
		assertTrue(item.display().equals(storage.getItem(name).display()));
	}
	
	@Test
	public void testItemMap() throws Exception {
		String name = "item ";
		Map<String , Item> itemMap = new HashMap<String, Item>();
		for(int i=0;i<10;i++) {
			itemMap.put(name+i,new Item(name +i, 1+i, 1.99f+(float)i, 3+i));
		}
		storage.setItemMap(itemMap); 
		
		assertTrue(itemMap.equals(storage.getItemMap()));
	}

}