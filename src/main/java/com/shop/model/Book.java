package com.shop.model;

public class Book  extends Item {
	public Book(Item item) {
		super(item.getName(),item.getId(),item.getPrice(),item.getNbrElt());
	}


	public Book() {
		super();
	}
	int nbPage; // nbre de pages du livre
	String author; // nom de l�auteur
	String publisher; // nom de l��diteur
	int year; // ann�e de publication
	int age;
	
	public Book(String name, int id, float price, int nbrElt,int nbPage,String author,String publisher,	int year,int age) {
		super(name, id, price, nbrElt);

		this.nbPage = nbPage; // nbre de pages du livre
		this.author =author; // nom de l�auteur
		this.publisher =publisher; // nom de l��diteur
		this.year =year; // ann�e de publication
		this.age=age;
	}


	public int getNbPage() {
		return nbPage;
	}


	public void setNbPage(int nbPage) {
		this.nbPage = nbPage;
	}


	public String getAuthor() {
		return author;
	}


	public void setAuthor(String author) {
		this.author = author;
	}


	public String getPublisher() {
		return publisher;
	}


	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}


	public int getYear() {
		return year;
	}


	public void setYear(int year) {
		this.year = year;
	}


	public int getAge() {
		return age;
	}


	public void setAge(int age) {
		this.age = age;
	}

}
