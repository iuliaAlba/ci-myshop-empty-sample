package com.shop.model;

public class PuzzleBook  extends Book {
	int nbrPieces;
	
	public PuzzleBook() {
		super();
	}
	
	public PuzzleBook(Book book) {
		super(book.getName(),book.getId(),book.getPrice(),book.getNbrElt(),book.getNbPage(),book.getAuthor(),book.getPublisher(),book.getYear(),book.getAge());
	}
	public PuzzleBook(String name, int id, float price, int nbrElt, int nbPage, String author, String publisher,
			int year, int age,int nbrPieces) {
		super(name, id, price, nbrElt, nbPage, author, publisher, year, age);
		// TODO Auto-generated constructor stub
		this.nbrPieces=nbrPieces;
	}

	public int getNbrPieces() {
		return nbrPieces;
	}

	public void setNbrPieces(int nbrPieces) {
		this.nbrPieces = nbrPieces;
	}
}
