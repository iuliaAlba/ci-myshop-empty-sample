package com.shop.model;

public class BookToTouch extends Book {
	String material ;// e.g tissue, feuille d�aluminium, laine etc�
	int durability ;// indiquant la dur�e de vie en mois du livre
	
	public BookToTouch() {
		super();
	}
	
	public BookToTouch(Book book) {
		super(book.getName(),book.getId(),book.getPrice(),book.getNbrElt(),book.getNbPage(),book.getAuthor(),book.getPublisher(),book.getYear(),book.getAge());
	}
	public BookToTouch(String name, int id, float price, int nbrElt, int nbPage, String author, String publisher,
			int year, int age,String material, int durability) {
		super(name, id, price, nbrElt, nbPage, author, publisher, year, age);
		// TODO Auto-generated constructor stub
		this.material=material;
		this.durability=durability;
	}

	public String getMaterial() {
		return material;
	}

	public void setMaterial(String material) {
		this.material = material;
	}

	public int getDurability() {
		return durability;
	}

	public void setDurability(int durability) {
		this.durability = durability;
	}

}
