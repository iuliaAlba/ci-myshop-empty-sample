package com.shop.model;

public class Paper extends Consumable {
	public Paper() {
		super();
	}
	public Paper(String name, int id, float price, int nbrElt, int quantity) {
		super(name, id, price, nbrElt, quantity);
	}
	public Paper(String name, int id, float price, int nbrElt, int quantity, String quality, float weight) {
		super(name, id, price, nbrElt, quantity);
		this.quality=quality;
		this.weight=weight;
	}
	String quality; //: indiquant la qualit� du papier (LOW, MEDIUM, HEIGH)
	float weight; //: indiquant le poids en gramme d�une feuille
	public String getQuality() {
		return quality;
	}
	public void setQuality(String quality) {
		this.quality = quality;
	}
	public float getWeight() {
		return weight;
	}
	public void setWeight(float weight) {
		this.weight = weight;
	}

}
