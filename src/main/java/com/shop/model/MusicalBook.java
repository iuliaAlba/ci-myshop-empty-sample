package com.shop.model;

import java.util.List;

public class MusicalBook extends Book {
	List<String> listOfSound ;//: Liste des chansons disponibles
	int lifetime ;//: indiquant la dur�e de vie en mois du livre
	int nbrBattery ;//: indiquant le nombre de piles n�cessaires.
	
	public MusicalBook() {
		super();
	}
	
	public MusicalBook(Book book) {
		super(book.getName(),book.getId(),book.getPrice(),book.getNbrElt(),book.getNbPage(),book.getAuthor(),book.getPublisher(),book.getYear(),book.getAge());
	}
	public MusicalBook(String name, int id, float price, int nbrElt, int nbPage, String author, String publisher,
			int year, int age,List<String> listOfSound, int lifetime,int nbrBattery) {
		super(name, id, price, nbrElt, nbPage, author, publisher, year, age);
		// TODO Auto-generated constructor stub
		this.listOfSound=listOfSound;
		this.lifetime=lifetime;
		this.nbrBattery=nbrBattery;
	}

	public List<String> getListOfSound() {
		return listOfSound;
	}

	public void setListOfSound(List<String> listOfSound) {
		this.listOfSound = listOfSound;
	}

	public int getLifetime() {
		return lifetime;
	}

	public void setLifetime(int lifetime) {
		this.lifetime = lifetime;
	}

	public int getNbrBattery() {
		return nbrBattery;
	}

	public void setNbrBattery(int nbrBattery) {
		this.nbrBattery = nbrBattery;
	}
}
