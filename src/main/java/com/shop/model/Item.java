package com.shop.model;

public class Item {
	public Item() {
		super();
	}
	public Item(String name, int id, float price, int nbrElt) {
		super();
		this.name = name;
		this.id = id;
		this.price = price;
		this.nbrElt = nbrElt;
	}
	private String name;
	private int id;
	private float price;
	private int nbrElt;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	public int getNbrElt() {
		return nbrElt;
	}
	public void setNbrElt(int nbrElt) {
		this.nbrElt = nbrElt;
	}
	public String display() {
		String toto="id = "+this.id+" , name = "+this.name+" , price = "+this.price+" , nbrElt = "+this.nbrElt;
		return toto;
	}
}
