package com.shop.model;

public class Pen extends Consumable {
	public Pen() {
		super();
	}
	public Pen(String name, int id, float price, int nbrElt, int quantity) {
		super(name, id, price, nbrElt, quantity);
	}
	public Pen(String name, int id, float price, int nbrElt, int quantity, String color, int durability) {
		super(name, id, price, nbrElt, quantity);
		this.color=color;
		this.durability=durability;
	}
	String color; 
	int durability; 
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public int getDurability() {
		return durability;
	}
	public void setDurability(int durability) {
		this.durability = durability;
	}

}
