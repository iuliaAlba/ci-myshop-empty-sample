package com.shop.model;

public class OriginalBook extends Book {
	boolean isNumeric;
	
	public OriginalBook() {
		super();
	}
	
	public OriginalBook(Book book) {
		super(book.getName(),book.getId(),book.getPrice(),book.getNbrElt(),book.getNbPage(),book.getAuthor(),book.getPublisher(),book.getYear(),book.getAge());
	}
	public OriginalBook(String name, int id, float price, int nbrElt, int nbPage, String author, String publisher,
			int year, int age,boolean isNumeric) {
		super(name, id, price, nbrElt, nbPage, author, publisher, year, age);
		// TODO Auto-generated constructor stub
		this.isNumeric=isNumeric;
	}

	public boolean isNumeric() {
		return isNumeric;
	}

	public void setNumeric(boolean isNumeric) {
		this.isNumeric = isNumeric;
	}

}
