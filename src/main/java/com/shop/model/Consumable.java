package com.shop.model;

public class Consumable extends Item {
	public Consumable(Item item) {
		super(item.getName(),item.getId(),item.getPrice(),item.getNbrElt());
	}


	public Consumable() {
		super();
	}
	private int quantity;
	public Consumable(String name, int id, float price, int nbrElt,int quantity) {
		super(name, id, price, nbrElt);
		// TODO Auto-generated constructor stub
		this.quantity =quantity;
	}

	
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

}
