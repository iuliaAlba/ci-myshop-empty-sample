package com.shop.starter;

import java.util.HashMap;
import java.util.Map;

import com.shop.controller.Shop;
import com.shop.controller.Storage;
import com.shop.model.Item;

public class Launcher {
	public static void main(String[] args) {

		System.out.println("Bienvenue dans le Shop de Julia et Adrien");
		System.out.println("----------");
		
		Map<String , Item> itemMap = new HashMap<String, Item>();
		Item item1 = new Item("concombre", 1, 3, 6);
		Item item2 = new Item("carotte", 2, 2, 5);
		Item item3 = new Item("courgette", 3, 4, 5);

		Storage storage = new Storage();
		storage.addItem(item1);
		storage.addItem(item2);
		
		Shop shop = new Shop(storage,1000);

		shop.buy(item3);
		shop.displayCash();

		shop.sell(item1.getName());
		
		System.out.println("Le montant de la caisse est : " + shop.getCash());

	}

}
