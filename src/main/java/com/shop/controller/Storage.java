package com.shop.controller;

import java.util.HashMap;
import java.util.Map;

import com.shop.model.Item;

public class Storage {
	private Map<String , Item> itemMap;
	
	public Storage() {
		super();
		this.itemMap = new HashMap<String, Item>();
	}
	
	
	public void addItem(Item obj) {
		this.itemMap.put(obj.getName(), obj);
	}

	public Item getItem(String name) {
		Item obj;
		obj = this.itemMap.get(name);
		return obj;
	}


	public Map<String, Item> getItemMap() {
		return itemMap;
	}


	public void setItemMap(Map<String, Item> itemMap) {
		this.itemMap = itemMap;
	}
}
