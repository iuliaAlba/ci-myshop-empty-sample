package com.shop.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import com.shop.model.Book;
import com.shop.model.Consumable;
import com.shop.model.Item;

public class Shop {

	private Storage storage;
	private float cash;

	public Shop() {
		super();
	}
	
	public Shop(Storage storage, float cash) {
		super();
		this.storage = storage;
		this.cash = cash;
	}
	
	public float getCash() {
		return cash;
	}

	public void setCash(float cash) {
		this.cash = cash;
	}

	public Storage getStorage() {
		return storage;
	}

	public void setStorage(Storage storage) {
		this.storage = storage;
	}
	
	public Item sell(String name) {
		Item item = this.storage.getItem(name);
		cash = cash + item.getPrice();
		storage.getItemMap().remove(name);
		return item;
	}
	
	public boolean buy(Item item) {
		boolean b=false;
		if(this.cash >= item.getPrice()) {
			b=true;
			this.storage.addItem(item);
			this.cash-=item.getPrice();
		}
		return b;
	}
	public String displayCash() {
		return "cash = "+ this.cash;
		
	}

	public boolean isItemAvailable(String name) {
		// permet de savoir si un Item est disponible
		boolean isIA=false;
		Item item = this.storage.getItem(name);
		if (item != null)isIA=true;
		return isIA;
	}
	public int getAgeForBook(String name) {
		// permet de r�cup�rer l�age min d�un livre. Attention il faut tester si l�item est un livre ou non.
		int age = -1;
		
		try {
		    Book book = (Book) this.storage.getItem(name);
		    // No exception: obj is of type C or IT MIGHT BE NULL!
		    if (book != null)age = book.getAge();
		} catch (ClassCastException e) {
			System.out.println("not a book: "+e);
		}
		return age;
	}
	public List<Book> getAllBook() {
		// permet de retourner tous les livres du Shop
		List<Book> lBook = new ArrayList<Book>();
        for (Entry<String, Item> entry : this.storage.getItemMap().entrySet()) {
            
            Item item = entry.getValue();
            try {
    		    Book book = (Book) item;
    		    // No exception: obj is of type C or IT MIGHT BE NULL!
    		    lBook.add(book);
    		} catch (ClassCastException e) {
    		}
        }
		return lBook;
	}
	public int getINbtemInStorage(String name) {
		// permet de retourner la quantit� d�un item disponible dans le Storage
		int qtItem=0;
	
        for (Entry<String, Item> entry : this.storage.getItemMap().entrySet()) {
            
            if( name.equals(entry.getKey())) {
            	qtItem+=this.storage.getItem(name).getNbrElt();
            }

        }
		return qtItem;
	}
	public int getQuantityPerConsumable(String name) {
		// retourne le nombre d��l�ment par lot.
		int nbElLot=-1;
		try {
		    Consumable consumable = (Consumable) this.storage.getItem(name);
		    nbElLot = consumable.getQuantity();
		    // No exception: obj is of type C or IT MIGHT BE NULL!
		} catch (ClassCastException e) {
			System.out.println("not a consumable: "+e);
		}
		return nbElLot;
	}

}
